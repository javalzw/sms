package com.zlkj.sms.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author lzw
 * @description webSocket配置类
 * @date 2021/1/21
 */
@Configuration
public class InitBeanConfig {

    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        return new ServerEndpointExporter();
    }

    @Bean("playTaskExecutor")
    public ThreadPoolTaskExecutor videoTaskExecutor() {
        return createThreadPool("sms-play");
    }

    @Bean("recordTaskExecutor")
    public ThreadPoolTaskExecutor recordTaskExecutor() {
        return createThreadPool("sms-record");
    }

    public static ThreadPoolTaskExecutor createThreadPool(String threadName){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 设置核心线程数
        executor.setCorePoolSize(50);
        // 设置最大线程数
        executor.setMaxPoolSize(200);
        // 设置队列容量
        executor.setQueueCapacity(20000);
        // 设置线程活跃时间（秒）
        executor.setKeepAliveSeconds(60);
        // 设置默认线程名称
        executor.setThreadNamePrefix(threadName);
        // 设置拒绝策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.DiscardPolicy());
        return executor;
    }
}
