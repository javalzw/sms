package com.zlkj.sms.startapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author lzw
 * @description 自建流媒体服务启动类
 * @date 2021/1/21
 */
@SpringBootApplication(scanBasePackages = {"com.zlkj.sms.*"})
@ServletComponentScan
@EnableAsync
public class StreamMediaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StreamMediaServerApplication.class, args);
    }

}
