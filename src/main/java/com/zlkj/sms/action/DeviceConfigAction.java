package com.zlkj.sms.action;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.zlkj.sms.bean.DeviceBean;
import com.zlkj.sms.bean.ResultData;
import com.zlkj.sms.util.Constanst;
import com.zlkj.sms.util.Tools;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.util.StringUtils;

import java.io.File;
import java.util.List;

/**
 * @author lzw
 * @description 视频设备信息配置控制器
 * @date 2021/1/21
 */
@Slf4j
@RestController
@RequestMapping("/device")
public class DeviceConfigAction {

    /**
     * 密码校验
     * @param password
     * @return
     */
    @PostMapping("/checkPassword")
    public ResultData checkPassword(String password){
        boolean flag = false;
        String mm = Tools.encryption(password);
        flag = Constanst.SYS_CIPHER_TEXT.equals(mm);
        return Tools.toResult(flag, "00");
    }

    /**
     * 读取设备配置数据列表
     * @return
     */
    @PostMapping("/list")
    public ResultData getDeviceList(){
        List<DeviceBean> devices = Tools.getDevices();
        return Tools.toResult(devices);
    }

    /**
     * 操作设备配置文件： 新增-02、修改-03、删除-04
     * @param device
     * @param gnbh
     * @return
     */
    @PostMapping("/operDevice")
    public ResultData operDeviceInfo(DeviceBean device, String gnbh){
        boolean flag = false, upState = false;
        List<DeviceBean> devices = Tools.getDevices();
        if(Constanst.SYS_INSERT.equals(gnbh) && devices != null){
            for(DeviceBean item : devices){
                if(Tools.strEq(item.getSerialNumber(), device.getSerialNumber())){
                    return new ResultData("-1", "设备已经存在");
                }
            }
            upState = true;
            flag = devices.add(device);
        }else if(Constanst.SYS_UPDATE.equals(gnbh) && devices != null){
            for(DeviceBean item : devices){
                if(Tools.strEq(item.getSerialNumber(), device.getSerialNumber())){
                    devices.remove(item);
                    flag = devices.add(device);
                    upState = true;
                    break;
                }
            }
        }else if(Constanst.SYS_DELETE.equals(gnbh) && devices != null){
            for(DeviceBean item : devices){
                if(Tools.strEq(item.getSerialNumber(), device.getSerialNumber())){
                    flag = devices.remove(item);
                    upState = true;
                    break;
                }
            }
        }

        if(!upState){
            return new ResultData("-1", "设备不存在");
        }

        if(flag){
            try{
                File file = new File(System.getProperty("user.dir").replaceAll("\\\\", "/") + "/db/device_data.json");
                FileUtil.writeString(JSON.toJSONString(devices), file, "UTF-8");
            }catch (Exception e){
                flag = false;
                log.error("设备配置文件更新异常：{}", e.getMessage());
            }
        }

        return Tools.toResult(flag, gnbh);
    }

    /**
     * 备份设备配置信息
     * @return
     */
    @RequestMapping("bakDevices")
    public ResultData bakDevicesInfo(){
        boolean flag = Tools.bakDevices();
        return Tools.toResult(flag, "00");
    }

    /**
     * 替换设备配置文件
     * @return
     */
    @PostMapping("/resetDevice")
    public ResultData resetDeviceFile(){
        ResultData result = new ResultData("-1", "替换设备配置文件失败");
        String filePath = System.getProperty("user.dir").replaceAll("\\\\", "/") + "/db/device_data.bak";
        File f1 = new File(filePath);
        if(f1.exists()){
            String res = FileUtil.readString(f1, "UTF-8");
            if(!StringUtils.isEmpty(res) && res.length() > 50){
                File f2 = new File(System.getProperty("user.dir").replaceAll("\\\\", "/") + "/db/device_data.json");
                FileUtil.writeString(res, f2, "UTF-8");
                result.setCode("1");
                result.setMsg("替换完成");
            }else{
                result.setMsg("原文件数据为空");
            }
        }else{
            result.setMsg("原文件不存在，请在程序同目录下放置设备原配置文件");
        }
        return result;
    }
}
