package com.zlkj.sms.action;

import com.zlkj.sms.util.Tools;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author lzw
 * @description 流媒体服务主控制器
 * @date 2021/1/21
 */
@Controller
public class SmsAction {

    @Value("${server.port}")
    private String serverPort;

    @GetMapping("/")
    public String main(){
        return "sms/device";
    }

    @GetMapping("/toPage")
    public String toPage(HttpServletRequest request, String url, String data){
        request.setAttribute("data", data);
        request.setAttribute("hostPort", Tools.stringSplicing(Tools.getLocalHostIP(), ":", serverPort));
        return url;
    }
}
