package com.zlkj.sms.action;

import cn.hutool.core.util.StrUtil;
import com.zlkj.sms.bean.DeviceBean;
import com.zlkj.sms.bean.ResultData;
import com.zlkj.sms.util.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lzw
 * @description 视频录像控制器
 * @date 2021/1/22
 */
@Slf4j
@RestController
@RequestMapping("/record")
public class VideoRecordAction {

    @Autowired()
    @Qualifier("recordTaskExecutor")
    private ThreadPoolTaskExecutor taskExecutor;


    /**
     * 开始录像
     * @param serialId
     * @param fileName
     * @return
     */
    @PostMapping("start")
    public ResultData startRecord(String serialId , String fileName){
        VideoRecord vr = VideoRecordTools.getVideoRecord(serialId);
        if(vr == null){
            DeviceBean device = Tools.getDeviceInfo(serialId);
            int width = device.getWidth() < 600 ? Constanst.VIDEO_WIDTH : device.getWidth();
            int height = device.getHeight() < 400 ? Constanst.VIDEO_HEIGHT : device.getHeight();
            vr = new VideoRecord(device.getStreamUrl(), serialId, width, height, fileName);
        }
        //开启录像线程
        taskExecutor.submit(vr);

        VideoRecordTools.addRecord(serialId, vr);
        return Tools.toResult(true, "00", vr.getFileName());
    }

    /**
     * 停止录像
     * @param serialId
     * @return
     */
    @PostMapping("stop")
    public ResultData stopRecord(String serialId){
        VideoRecord vr = VideoRecordTools.getVideoRecord(serialId);
        vr.setRecordState(false);
        VideoRecordTools.removeRecord(serialId);

        return Tools.toResult(true, "00", vr.getFileName());
    }

    /**
     * 批量停止录像
     * @param serialIds
     * @return
     */
    @PostMapping("stopRecords")
    public ResultData stopRecords(String serialIds){
        List<String> list = new ArrayList<>();
        if(StrUtil.isNotBlank(serialIds)){
            String[] arr = serialIds.split(",");
            for(String serialId : arr){
                VideoRecord vr = VideoRecordTools.removeRecord(serialId);
                if(vr != null){
                    list.add(vr.getFileName());
                }
            }
        }
        return Tools.toResult(true, "00", list);
    }

    /**
     * 根据指定路径读取视频文件
     * @param response
     * @param filePath
     */
    @PostMapping("getVideo")
    public void getVideo(HttpServletResponse response, String filePath){
        ServletOutputStream out = null;
        try{
            out = response.getOutputStream();
            byte[] video = Tools.getFileIO(new File(filePath));
            out.write(video);
        }catch (IOException e){
            log.error("读取视频流出现异常：{}", e.getMessage());
        }finally {
            Tools.closeOutputIO(out);
        }
    }

    /**
     * 停止预览视频推流
     * @param serialIds
     * @return
     */
    @PostMapping("stopVideoPlay")
    public ResultData stopVideoPlay(String serialIds){
        if(StrUtil.isNotBlank(serialIds)){
            String[] arr = serialIds.split(",");
            for(String serialId : arr){
                PushStreamTools.removeVideo(serialId);
            }
        }
        return Tools.toResult(true, "00");
    }
}
