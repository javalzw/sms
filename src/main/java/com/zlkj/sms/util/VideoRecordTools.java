package com.zlkj.sms.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lzw
 * @description 视频录像工具类
 * @date 2021/1/22
 */
public class VideoRecordTools {

    private static Map<String, VideoRecord> recordMap = new HashMap<>();

    /**
     * 添加录像线程
     * @param serialId
     * @param record
     */
    public static void addRecord(String serialId, VideoRecord record){
        if(recordMap.containsKey(serialId)){
            //移除并结束旧的推流线程
            VideoRecord v = recordMap.remove(serialId);
            v.setRecordState(false);
            recordMap.put(serialId, record);
        }else{
            recordMap.put(serialId, record);
        }
    }

    /**
     * 获取录像线程
     * @param serialId
     * @return
     */
    public static VideoRecord getVideoRecord(String serialId){
        return recordMap.get(serialId);
    }


    /**
     * 移除并结束录像线程
     * @param serialId
     */
    public static VideoRecord removeRecord(String serialId){
        VideoRecord vr = recordMap.remove(serialId);
        if(vr != null){
            vr.setRecordState(false);
        }
        return vr;
    }
}
