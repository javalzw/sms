package com.zlkj.sms.util;

/**
 * @author lzw
 * @description 文件类型枚举
 * @date 2020/12/15
 */
public enum FileType {

    /**
     * IMAGE 图片
     * CARD_IMAGE 证件照
     * AUDIO 语音
     * ITEM_AUDIO 指令语音
     * VIDEO 视频
     */
    IMAGE, CARD_IMAGE, AUDIO, ITEM_AUDIO, VIDEO;

}
