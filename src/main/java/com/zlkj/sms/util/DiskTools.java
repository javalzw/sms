package com.zlkj.sms.util;

import java.io.File;

/**
 * @author lzw
 * @description 系统磁盘管理工具类
 * @date 2020/12/31
 */
public class DiskTools {

    private static final String SYSTEM_PATH = "C:#D:";

    /**
     * 获取可用空间大于10G的磁盘
     * @return
     */
    public static String getDriveLetter(){
        String rootPath = "C:";
        // 磁盘使用情况
        File[] files = File.listRoots();
        for(File file : files){
            double free = file.getUsableSpace() * 1.0 / 1024 / 1024 / 1024;
            String path = file.getPath().replace("\\", "");
            if(!SYSTEM_PATH.contains(path) && free > 10){
                rootPath = path;
                break;
            }
        }
        return rootPath;
    }
}
