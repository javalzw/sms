package com.zlkj.sms.util;

/**
 * @author lzw
 * @description 功能操作枚举
 * @date 2021/1/21
 */
public enum GnMessage {

    CZ("00","操作成功","操作失败"),
    CX("01","数据获取成功","数据获取失败"),
    BC("02","保存成功","保存失败"),
    XG("03","修改成功","修改失败"),
    SC("04","删除成功","删除失败");

    private String gnbj;
    private String successMsg;
    private String failMsg;

    private GnMessage(String gnbj, String successMsg, String failMsg){
        this.gnbj = gnbj;
        this.successMsg = successMsg;
        this.failMsg = failMsg;
    }

    /**
     * 成功提示
     * @param gnbj
     * @return
     */
    public static String getSuccessMsg(String gnbj){
        for(GnMessage gn : GnMessage.values()){
            if(gn.gnbj.equals(gnbj)){
                return gn.successMsg;
            }
        }
        return "操作成功";
    }

    /**
     * 失败提示
     * @param gnbj
     * @return
     */
    public static String getFailMsg(String gnbj){
        for(GnMessage gn : GnMessage.values()){
            if(gn.gnbj.equals(gnbj)){
                return gn.failMsg;
            }
        }
        return "操作失败";
    }

    public String getGnbj() {
        return gnbj;
    }

    public void setGnbj(String gnbj) {
        this.gnbj = gnbj;
    }

    public String getSuccessMsg() {
        return successMsg;
    }

    public void setSuccessMsg(String successMsg) {
        this.successMsg = successMsg;
    }

    public String getFailMsg() {
        return failMsg;
    }

    public void setFailMsg(String failMsg) {
        this.failMsg = failMsg;
    }

}
