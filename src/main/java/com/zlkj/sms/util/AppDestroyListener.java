package com.zlkj.sms.util;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.zlkj.sms.bean.DeviceBean;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

/**
 * @author lzw
 * @description 服务注销监听类
 * @date 2021/1/22
 */
@Component
public class AppDestroyListener implements DisposableBean {

    @Override
    public void destroy(){
        Tools.bakDevices();
    }

}
