package com.zlkj.sms.util;

/**
 * @author lzw
 * @description 流媒体服务常量类
 * @date 2021/1/21
 */
public class Constanst {

    /**
     * 查询功能编号
     */
    public static final String SYS_SELECT = "01";
    /**
     * 新增功能编号
     */
    public static final String SYS_INSERT = "02";
    /**
     * 修改功能编号
     */
    public static final String SYS_UPDATE = "03";
    /**
     * 删除功能编号
     */
    public static final String SYS_DELETE = "04";

    /**
     * 系统检验密文
     */
    public static final String SYS_CIPHER_TEXT = "B5DDC4258843690940C5993FEE60681A";

    /**
     * 视频默认宽度
     */
    public static final int VIDEO_WIDTH = 600;

    /**
     * 视频默认高度
     */
    public static final int VIDEO_HEIGHT = 400;
}
