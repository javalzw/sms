package com.zlkj.sms.util;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lzw
 * @description rtsp转rtmp工具类
 * @date 2021/1/21
 */
@Slf4j
public class PushStreamTools {

    private static Map<String, VideoPlay> videoMap = new HashMap<>();

    /**
     * 添加推流线程
     * @param serialId
     * @param video
     */
    public static void addVideo(String serialId, VideoPlay video){
        if(videoMap.containsKey(serialId)){
            //移除并结束旧的推流线程
            VideoPlay v = videoMap.remove(serialId);
            v.setPushState(false);
            videoMap.put(serialId, video);
        }else{
            videoMap.put(serialId, video);
        }
    }


    /**
     * 移除并结束推流线程
     * @param serialId
     */
    public static void removeVideo(String serialId){
        VideoPlay v = videoMap.remove(serialId);
        v.setPushState(false);
    }

}
