package com.zlkj.sms.bean;

import lombok.Data;
import lombok.NonNull;

/**
 * @author lzw
 * @description 请求响应数据封装实体类
 * @date 2021/1/21
 */
@Data
public class ResultData {

    @NonNull
    private String code;
    @NonNull
    private String msg;
    private Object data;

}
