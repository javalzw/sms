package com.zlkj.sms.bean;

import lombok.Data;

/**
 * @author lzw
 * @description 视频设备配置实体类
 * @date 2021/1/21
 */
@Data
public class DeviceBean {

    private String serialNumber;
    private String deviceName;
    private String streamUrl;
    private String channel;
    private int width;
    private int height;
    private String state;
}
