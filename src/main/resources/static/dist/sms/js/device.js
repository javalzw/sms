layui.use(['layer', 'table'], function(){
    var layer = layui.layer, table = layui.table, active = false, key = '407e9ac2c88e34af227da1eb28253f74', gnbh = '02', state = '1', ope = '0';

    layer.prompt({
        title: '用户验签',
        formType: 1,
        btn: ['确认']
    }, function(value, index, elem){
        var password = md5(value + key);
        $.ajax({
            url: 'device/checkPassword',
            type: 'POST',
            data: {password:password},
            dataType: 'json',
            success: function(res){
                if(res && res.code == '1'){
                    layer.close(index);
                    active = true;
                    $("#deviceForm").attr('style', 'display:block;');
                    $("#refreshBtn").html('<a href="javascript:;" class="layui-btn layui-btn-sm" id="refresh"><i class="layui-icon">&#xe9aa;</i>刷新</a>');
                    getDeviceList();
                }else{
                    layer.alert('验签失败', {title:'信息提示', icon:2});
                }
            },
            error: function(){
                layer.alert('程序执行出现异常', {title:'信息提示', icon:2});
            }
        });
    });


    //新增设备
    $("#add").click(function(){
        if(active){
            operDevice();
        }else{
            layer.alert('请刷新页面重新验签', {title:'信息提示', icon:2});
        }
    });

    //重置表单
    $("#reset").click(function(){
        resetForm();
    });

    //刷新列表
    $("#refreshBtn").on('click', '#refresh',function(){
        getDeviceList();
    });

    //导入配置文件
    $("#resetFile").click(function(){
        if(active){
            layer.confirm('您确认在程序同目录db文件夹已经放置了原配置文件？', {icon: 3, title:'操作提示'}, function(index){
                $.ajax({
                    url: 'device/resetDevice',
                    type: 'POST',
                    dataType: 'json',
                    success: function(res){
                        if(res && res.code == '1'){
                            layer.msg(res.msg||'配置文件替换成功', {icon:1});
                        }else{
                            layer.alert(res.msg||'配置文件替换失败', {title:'信息提示', icon:2});
                        }
                    },
                    error: function(){
                        layer.alert('程序执行出现异常', {title:'信息提示', icon:2});
                    }
                });
                layer.close(index);
            });
        }else{
            layer.alert('请刷新页面重新验签', {title:'信息提示', icon:2});
        }
    });

    //导入配置文件
    $("#bakFile").click(function(){
        if(active){
            $.ajax({
                url: 'device/bakDevices',
                type: 'POST',
                dataType: 'json',
                success: function(res){
                    if(res && res.code == '1'){
                        layer.msg('配置文件备份成功', {icon:1});
                    }else{
                        layer.alert('配置文件备份失败', {title:'信息提示', icon:2});
                    }
                },
                error: function(){
                    layer.alert('程序执行出现异常', {title:'信息提示', icon:2});
                }
            });
        }else{
            layer.alert('请刷新页面重新验签', {title:'信息提示', icon:2});
        }
    });

    //列表数据
    function getDeviceList(){
        table.render({
            elem: '#dataList'
            ,url: 'device/list' //数据接口
            ,method: 'POST'
            ,height: 560
            ,cols: [[ //表头
                {type: 'numbers', title: '序号', width:'5%', align:'center'}
                ,{field: 'serialNumber', title: '设备序列号', width:'12%', align:'center'}
                ,{field: 'deviceName', title: '设备名称', width:'10%', align:'center'}
                ,{field: 'streamUrl', title: 'RTSP流地址', width:'18%', align:'center', templet: function(data){
                    var url = (data.streamUrl||'');
                    url = url.lastIndexOf('@') == -1 ? url : 'rtsp://******'+url.substring(url.lastIndexOf('@')+1);
                    return url;
                }}
                ,{field: 'channel', title: '通道号', width:'6%', align:'center'}
                ,{field: 'width', title: '视频宽度', width:'8%', align:'center'}
                ,{field: 'height', title: '视频高度', width:'8%', align:'center'}
                ,{field: 'state', title: '状态', width:'6%', align:'center', templet: function(data){
                    return data.state == '1' ? '<span style="color: #09d709; font-weight: bold">启用</span>' : '<span style="color: #b2b2b2; font-weight: bold">停用</span>';
                }}
                ,{field: 'oper', title: '操作', width:'27%', align:'center', toolbar:'#operBar'}
            ]]
        });
    }

    //操作栏按钮监听
    table.on('tool(dataList)', function(obj){
        var data = obj.data, layEvent = obj.event;
        var serialNumber = data.serialNumber;

        if(layEvent == 'edit'){
            gnbh = '03';
            $("#serialNumber").val(serialNumber);
            $("#streamUrl").val(data.streamUrl);
            $("#deviceName").val(data.deviceName);
            $("#channel").val(data.channel);
            $("#width").val(data.width);
            $("#height").val(data.height);
        }else if(layEvent == 'delete'){
            layer.confirm('您确定要删除【'+serialNumber+'】设备吗？', {icon: 3, title:'操作提示'}, function(index){
                gnbh = '04';
                $("#serialNumber").val(serialNumber);
                operDevice();
            });
        }else if(layEvent == 'play'){
            videoPlay(serialNumber);
        }else if(layEvent == 'record'){
            var opeBtn = obj.tr.find('.record');
            var params = {serialId:serialNumber, fileName: "测试_"+(new Date().getTime())};
            ope = ope == '1' ? '2' : '1';
            opeBtn.html('<i class="layui-icon">&#xe60e;</i>'+(ope == '1' ? '关闭录像' : '开始录像'));
            opeBtn.attr('style','background-color:'+(ope == '1' ? '#FF5722' : '#009688'));
            videoRecord(params, ope);
        }else if(layEvent == 'use'){
            gnbh = '03';
            state = data.state == '1' ? '0' : '1';
            $("#serialNumber").val(data.serialNumber);
            $("#streamUrl").val(data.streamUrl);
            $("#channel").val(data.channel);
            operDevice();
        }
    });

    //视频录像操作
    function videoRecord(params, ope) {
        var url = ope == '1' ? 'record/start' : 'record/stop';
        $.ajax({
            url: url,
            type: 'POST',
            data: params,
            dataType: 'json',
            success: function(res){
                if(res && res.code == '1'){
                    $("#record").html('<i class="layui-icon">&#xe60e;</i>'+(ope == '1' ? '关闭录像' : '开始录像'));
                    layer.msg(res.msg||'操作成功', {icon: 1});
                    ope = '0';
                }else{
                    layer.alert(res.msg||'操作失败', {title:'信息提示', icon: 2});
                }
            },
            error: function(){
                layer.alert('程序执行异常', {title:'信息提示', icon: 2});
            }
        });
    }

    //视频预览
    function videoPlay(serialNumber) {
        layer.open({
            type: 2,
            title: '视频实时预览',
            content: 'toPage?url=sms/video_play&data='+serialNumber,
            area: ['800px', '550px'],
            resize: false
        });
    }

    //操作设备信息
    function operDevice(){
        var params = {
            serialNumber: $("#serialNumber").val()||'',
            deviceName: $("#deviceName").val()||'',
            streamUrl: $("#streamUrl").val()||'',
            channel: $("#channel").val()||'',
            width: $("#width").val()||'600',
            height: $("#height").val()||'400',
            state: state,
            gnbh: gnbh
        }

        if(checkForm(params)){
            $.ajax({
                url: 'device/operDevice',
                type: 'POST',
                data: params,
                dataType: 'json',
                success: function(res){
                    if(res && res.code == '1'){
                        layer.msg(res.msg||'操作成功', {icon: 1});
                        resetForm();
                        getDeviceList();
                    }else{
                        layer.alert(res.msg||'操作失败', {title:'信息提示', icon: 2});
                    }
                },
                error: function(){
                    layer.alert('程序执行异常', {title:'信息提示', icon: 2});
                }
            });
        }
    }

    function checkForm(params) {
        if(!params.serialNumber){
            layer.msg('设备序列号不能为空', {icon: 0});
            return false;
        }

        var url = (params.streamUrl||'').toUpperCase();
        if(params.gnbh != '04' && !url){
            layer.msg('RTSP流地址不能为空', {icon: 0});
            return false;
        }else if(params.gnbh != '04' && url.indexOf('RTSP://') == -1){
            layer.msg('RTSP流地址格式不正确', {icon: 0});
            return false;
        }
        if(!params.channel && params.gnbh != '04'){
            layer.msg('设备通道号不能为空', {icon: 0});
            return false;
        }

        return true;
    }

    function resetForm(){
        gnbh = '02';
        $("#serialNumber").val('');
        $("#deviceName").val('');
        $("#streamUrl").val('');
        $("#channel").val('');
        $("#width").val('600');
        $("#height").val('400');
    }
});