# SMS

#### 介绍
对支持RTSP协议的摄像机的协议转webSocket实现HTML实时预览播放，并且可以对视频进行录像操作

#### 软件架构
该服务架构采用SpringBoot+javacv+ffmpeg实现

#### 安装教程

1.  将下载好的项目打包成jar包 通过命令运行，外置Tomcat环境运行打包成war包

#### 使用说明
1.  rtsp流地址常见格式：rtsp://设备账号:设备密码@设备IP:554/stream1，rtsp://设备账号:设备密码@设备IP:554/h264/ch1/main/av_stream，实际以摄像机产品为准
2.  在浏览器输入：http://ip:port；直接回车访问，页面加载完成需要做验签，在验签弹层输入zlkj@sms完成验签（在代码中可自行修改密码），配置好摄像机rtsp协议取流地址就可以测试播放、录像了。
3.  用户验签
    ![输入图片说明](file/img/1676274054477.jpg)
4.  设备对接列表
    ![输入图片说明](file/img/1676274072017.jpg)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
